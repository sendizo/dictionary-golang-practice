package dictionary

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Words struct {
	Word string `json:"word"`
	Def  string `json:"def"`
}

//
func (db *WordStoreRedis) Save(c echo.Context) error {
	w := Words{}
	if err := c.Bind(&w); err != nil {
		return c.JSON(http.StatusBadRequest, "Bad Request")
	}
	db.redis.Set(w.Word, w.Def, -1)
	return c.JSON(http.StatusCreated, w)
}
func (db *WordStoreRedis) Delete(c echo.Context) error {
	word := c.Param("word")
	db.redis.Del(word)
	return c.JSON(http.StatusCreated, word)
}
func (db *WordStoreRedis) Search(c echo.Context) error {
	word := c.Param("word")
	ok, res := db.lookup(word)
	if !ok {
		msg := fmt.Sprint("unable to find : " + word)
		return c.JSON(http.StatusCreated, msg)
	}
	return c.JSON(http.StatusCreated, res)
}

type EditDef struct {
	NewDef string `json:"new_def"`
}

func (db *WordStoreRedis) Update(c echo.Context) error {
	nd := EditDef{}
	if err := c.Bind(&nd); err != nil {
		return c.JSON(http.StatusBadRequest, "Bad Request")
	}
	word := c.Param("word")
	ok, _ := db.lookup(word)
	if !ok {
		msg := fmt.Sprint("unable to find : " + word)
		return c.JSON(http.StatusCreated, msg)
	}
	db.redis.Set(word, nd.NewDef, -1)
	msg := fmt.Sprint("Great! Word Has been updated")
	return c.JSON(http.StatusCreated, msg)
}

//
func (db *WordStoreRedis) lookup(word string) (bool, string) {
	results, err := db.redis.Get(word).Result()
	if err != nil {
		return false, results
	}
	return true, results
}
