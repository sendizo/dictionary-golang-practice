package dictionary

import (
	"github.com/go-redis/redis"
)

type WordStoreRedis struct {
	redis *redis.Client
}

func NewWordStoreRedis(redis *redis.Client) *WordStoreRedis {
	return &WordStoreRedis{redis: redis}
}
