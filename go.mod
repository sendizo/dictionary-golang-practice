module DictionaryApp

go 1.12

require (
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.9
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
)
