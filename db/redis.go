package db

import (
	"github.com/go-redis/redis"
)

func NewStoreRedis(addr, port, pass string) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr + ":" + port,
		Password: pass, // no password set
		DB:       7,    // use default DB
	})
	//
	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}

	return client, nil
}
