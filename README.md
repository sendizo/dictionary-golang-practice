# Dictionary App Go

This is a small CRUD Dictionary.

* Add words and definiations.
* Look up words.
* Edit Word.
* Delete word.

Using Redis as in-memory database to store Words & Definitions.


# How to use it on Docker.

* 1. Create .env file.

##################################

COMPOSE_PROJECT_NAME=listcheck-app

##

DB_ROOT_PASSWORD=???

DB_NAME=testdb

DB_USERNAME=testuser

DB_PASSWORD=???

###

PG_NAME=psdata

PG_USERNAME=psuser

PG_PASSWORD=???

###

REDIS_HOSTS=redis

REDIS_PASSWORD=???

##
##

WWW_ROOT=./volumes/www

############################

* Run docker-compose up
