package main

import (
	"DictionaryApp/db"
	"DictionaryApp/dictionary"
	"log"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	// Init Redis
	redis, err := db.NewStoreRedis("redis", "6379", "redispass")
	if err != nil {
		log.Println("Error Connecting to Redis")
	}
	// New Redis Store Service
	redisDB := dictionary.NewWordStoreRedis(redis)

	// Routes
	e.POST("/add", redisDB.Save)
	e.DELETE("/delete/:word", redisDB.Delete)
	e.GET("/search/:word", redisDB.Search)
	e.PUT("/update/:word", redisDB.Update)
	//
	e.Logger.Fatal(e.Start(":81"))
}
